import random
from tkinter import *
from sys import exit
import keyboard as key
from time import sleep

while True:
    def btn2():
        exit()


    import pygame


    def btn():
        w = 640
        h = 480
        hp = 100
        xp = 0
        bhp = 5000
        bs = 50
        FPS = 50
        sila = 10
        xs = 10
        ys = 10
        pygame.init()
        display = pygame.display.set_mode((640, 480))
        pygame.display.update()
        pygame.display.set_caption("Mega Boss 2.1")
        ex = False
        snake_pos = {
            "x": w / 2 - 10,
            "y": h / 2 - 100,
            "y_change": 0,
            "x_change": 0
        }
        snake_size = (10, 10)
        colors = {
            "hel": (255, 0, 255),
            "leh": (0, 255, 255),
            "dem": (255, 255, 255),
            "app": (255, 255, 0),
            "bhc": (0, 255, 0),
            "mes": (100, 100, 255)
        }
        food_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10
        }
        odinp_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10
        }
        yad_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10
        }
        boss_pos = {
            "x": 640 / 2 - 5,
            "y": 480 / 2 - 5
        }
        sila_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        m1_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        m2_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        m3_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        m4_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        m5_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        h1_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        print("\n")
        print("Your hp - ", hp)
        print("Your xp - ", xp)
        print("Boss hp - ", bhp)
        print("\n")
        clock = pygame.time.Clock()
        while not ex:
            try:
                for e in pygame.event.get():
                    if e.type == pygame.QUIT:
                        ex = True
                    elif e.type == pygame.KEYDOWN:
                        if e.key == pygame.K_LEFT:
                            snake_pos["x"] -= 10
                        if e.key == pygame.K_RIGHT:
                            snake_pos["x"] += 10
                        if e.key == pygame.K_UP:
                            snake_pos["y"] -= 10
                        if e.key == pygame.K_DOWN:
                            snake_pos["y"] += 10
                if key.is_pressed("p"):
                    input("PAUSE")
                    sleep(3)
            except:
                pass
            display.fill((0, 0, 0))
            boss = pygame.draw.circle(display, colors["bhc"], [boss_pos["x"], boss_pos["y"]], bs)
            player = pygame.draw.rect(display, colors["app"], [snake_pos["x"], snake_pos["y"], xs, ys])
            mesa = pygame.draw.circle(display, colors["mes"], [odinp_pos["x"], odinp_pos["y"]], 10)
            pygame.draw.rect(display, colors["dem"], [h1_pos["x"], h1_pos["y"], 10, 10])
            if (snake_pos["x"] < -snake_size[0]):
                snake_pos["x"] = 640 / 2 - 10
            elif (snake_pos["x"] > w):
                snake_pos["x"] = 640 / 2 - 10
            elif (snake_pos["y"] < -snake_size[1]):
                snake_pos["y"] = 480 / 2 - 10
            elif (snake_pos["y"] > h):
                snake_pos["y"] = 480 / 2 - 10
            randin2 = random.randint(1, 4)
            if randin2 == 1:
                odinp_pos["x"] -= 10
            if randin2 == 2:
                odinp_pos["x"] += 10
            if randin2 == 3:
                odinp_pos["y"] -= 10
            if randin2 == 4:
                odinp_pos["y"] += 10
            if bs <= 10:
                randin3 = random.randint(1, 4)
                if randin3 == 1:
                    boss_pos["x"] -= 10
                if randin3 == 2:
                    boss_pos["x"] += 10
                if randin3 == 3:
                    boss_pos["y"] -= 10
                if randin3 == 4:
                    boss_pos["y"] += 10

                if (boss_pos["x"] < -snake_size[0]):
                    boss_pos["x"] = w
                elif (boss_pos["x"] > w):
                    boss_pos["x"] = 0
                elif (odinp_pos["y"] < -snake_size[1]):
                    boss_pos["y"] = h
                elif (odinp_pos["y"] > h):
                    boss_pos["y"] = 0

            if (odinp_pos["x"] < -snake_size[0]):
                odinp_pos["x"] = w
            elif (odinp_pos["x"] > w):
                odinp_pos["x"] = 0
            elif (odinp_pos["y"] < -snake_size[1]):
                odinp_pos["y"] = h
            elif (odinp_pos["y"] > h):
                odinp_pos["y"] = 0
            pygame.draw.circle(display, colors["bhc"], [yad_pos["x"], yad_pos["y"]], 1)
            pygame.draw.circle(display, colors["leh"], [m1_pos["x"], m1_pos["y"]], 10)
            m1_pos = {
                "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
            }
            pygame.draw.circle(display, colors["leh"], [m2_pos["x"], m2_pos["y"]], 10)
            m2_pos = {
                "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
            }
            pygame.draw.circle(display, colors["leh"], [m3_pos["x"], m3_pos["y"]], 10)
            m3_pos = {
                "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
            }
            pygame.draw.circle(display, colors["leh"], [m4_pos["x"], m4_pos["y"]], 10)
            m4_pos = {
                "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
            }
            pygame.draw.circle(display, colors["leh"], [m5_pos["x"], m5_pos["y"]], 10)
            m5_pos = {
                "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
            }
            pygame.draw.rect(display, colors["hel"], [food_pos["x"], food_pos["y"], 10, 10])
            pygame.draw.rect(display, colors["dem"], [sila_pos["x"], sila_pos["y"], 5, 5])
            if snake_pos["x"] == food_pos["x"] and snake_pos["y"] == food_pos["y"]:
                hp += 5
                xp += 10
                print("Your xp - ", xp)
                print("Your hp - ", hp)
                colors["app"] = random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
                food_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
                yad_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10
                }
            if snake_pos["x"] == yad_pos["x"] and snake_pos["y"] == yad_pos["y"]:
                hp -= 100
                print("Green circle")
            if snake_pos["x"] == boss_pos["x"] and snake_pos["y"] == boss_pos["y"]:
                hp -= 70
                print("Your hp - ", hp)
            if snake_pos["x"] == sila_pos["x"] and snake_pos["y"] == sila_pos["y"]:
                sila += 2.5
                xp += 10
                print("Your xp - ", xp)
                print("You are strong!")
                sila_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == h1_pos["x"] and snake_pos["y"] == h1_pos["y"]:
                bhp -= sila
                print("Boss hp - ", bhp)
                h1_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == m1_pos["x"] and snake_pos["y"] == m1_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m1_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == m2_pos["x"] and snake_pos["y"] == m2_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m2_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == m3_pos["x"] and snake_pos["y"] == m3_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m3_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == m4_pos["x"] and snake_pos["y"] == m4_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m4_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == m5_pos["x"] and snake_pos["y"] == m5_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m5_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == odinp_pos["x"] and snake_pos["y"] == odinp_pos["y"]:
                hp -= 20
                print("Your hp - ", hp)

            if snake_pos["x"] == boss_pos["x"] and snake_pos["y"] == odinp_pos["y"]:
                hp -= 20
                print("Your hp - ", hp)
            if hp <= 0:
                print("You lose!")
                pygame.quit()
                ex = True
            if bhp <= 0:
                bs = 0
                sleep(1)
                print("You win!")
                pygame.quit()
                ex = True
            if bhp <= 500:
                bs = 25
                FPS = 100
            if bhp <= 250:
                bs = 10
            if bhp <= 70:
                bs = 5
                FPS = 500

            if xp == 100:
                sila += 2
                hp += 50
                xp += 1
                print("New level! (1)")
                print("You are strong!")
                print("Your hp - ", hp)

            if xp == 391:
                xp -= 1

            if xp == 400:
                sila += 3
                hp += 60
                xp += 1
                print("New level! (2)")
                print("You are strong!")
                print("Your hp - ", hp)

            if xp == 791:
                xp -= 1

            if xp == 800:
                sila += 4
                hp += 70
                xp += 1
                print("New level! (3)")
                print("You are strong!")
                print("Your hp - ", hp)

            if xp == 1591:
                xp -= 1

            if xp == 1600:
                sila += 5
                hp += 80
                xp += 1
                print("New level! (4)")
                print("You are strong!")
                print("Your hp - ", hp)

            if xp == 2991:
                xp -= 1

            if xp == 3000:
                sila += 6
                hp += 90
                xp += 1
                print("New level! (5)")
                print("You are strong!")
                print("Your hp - ", hp)

            clock.tick(FPS)
            try:
                pygame.display.update()
            except:
                pass
        pygame.quit()


    def btn3():
        w = 640
        h = 480
        hp = 100
        xp = 0
        bhp = 5000
        bs = 50
        FPS = 50
        sila = 10
        xs = 10
        ys = 10
        pygame.init()
        display = pygame.display.set_mode((640, 480))
        pygame.display.update()
        pygame.display.set_caption("Mega Boss 2.1")
        ex = False
        snake_pos = {
            "x": w / 2 - 10,
            "y": h / 2 - 100,
            "y_change": 0,
            "x_change": 0
        }
        snake_pos2 = {
            "x": w / 2 - 10,
            "y": h / 2 - 100,
            "y_change": 0,
            "x_change": 0
        }
        snake_size = (10, 10)
        colors = {
            "hel": (255, 0, 255),
            "leh": (0, 255, 255),
            "dem": (255, 255, 255),
            "app": (255, 255, 0),
            "bhc": (0, 255, 0),
            "mes": (100, 100, 255)
        }
        food_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10
        }
        odinp_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10
        }
        yad_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10
        }
        boss_pos = {
            "x": 640 / 2 - 5,
            "y": 480 / 2 - 5
        }
        sila_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        m1_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        m2_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        m3_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        m4_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        m5_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        h1_pos = {
            "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
            "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
        }
        fz = (10, 10)
        fe = 0
        print("\n")
        print("Your hp - ", hp)
        print("Your xp - ", xp)
        print("Boss hp - ", bhp)
        print("\n")
        clock = pygame.time.Clock()
        while not ex:
            try:
                for e in pygame.event.get():
                    if e.type == pygame.QUIT:
                        ex = True
                    elif e.type == pygame.KEYDOWN:
                        if e.key == pygame.K_LEFT:
                            snake_pos["x"] -= 10
                        if e.key == pygame.K_RIGHT:
                            snake_pos["x"] += 10
                        if e.key == pygame.K_UP:
                            snake_pos["y"] -= 10
                        if e.key == pygame.K_DOWN:
                            snake_pos["y"] += 10

                        if e.key == pygame.K_a:
                            snake_pos2["x"] -= 10
                        if e.key == pygame.K_d:
                            snake_pos2["x"] += 10
                        if e.key == pygame.K_w:
                            snake_pos2["y"] -= 10
                        if e.key == pygame.K_s:
                            snake_pos2["y"] += 10
                if key.is_pressed("p"):
                    input("PAUSE")
                    sleep(3)
            except:
                pass
            display.fill((0, 0, 0))
            boss = pygame.draw.circle(display, colors["bhc"], [boss_pos["x"], boss_pos["y"]], bs)
            player = pygame.draw.rect(display, colors["app"], [snake_pos["x"], snake_pos["y"], xs, ys])
            player2 = pygame.draw.rect(display, colors["app"], [snake_pos2["x"], snake_pos2["y"], xs, ys])
            mesa = pygame.draw.circle(display, colors["mes"], [odinp_pos["x"], odinp_pos["y"]], 10)
            pygame.draw.rect(display, colors["dem"], [h1_pos["x"], h1_pos["y"], 10, 10])
            if (snake_pos["x"] < -snake_size[0]):
                snake_pos["x"] = 640 / 2 - 10
            elif (snake_pos["x"] > w):
                snake_pos["x"] = 640 / 2 - 10
            elif (snake_pos["y"] < -snake_size[1]):
                snake_pos["y"] = 480 / 2 - 10
            elif (snake_pos["y"] > h):
                snake_pos["y"] = 480 / 2 - 10

            if (snake_pos2["x"] < -snake_size[0]):
                snake_pos2["x"] = 640 / 2 - 10
            elif (snake_pos2["x"] > w):
                snake_pos2["x"] = 640 / 2 - 10
            elif (snake_pos2["y"] < -snake_size[1]):
                snake_pos2["y"] = 480 / 2 - 10
            elif (snake_pos2["y"] > h):
                snake_pos2["y"] = 480 / 2 - 10

            randin2 = random.randint(1, 4)
            if randin2 == 1:
                odinp_pos["x"] -= 10
            if randin2 == 2:
                odinp_pos["x"] += 10
            if randin2 == 3:
                odinp_pos["y"] -= 10
            if randin2 == 4:
                odinp_pos["y"] += 10
            if bs <= 10:
                randin3 = random.randint(1, 4)
                if randin3 == 1:
                    boss_pos["x"] -= 10
                if randin3 == 2:
                    boss_pos["x"] += 10
                if randin3 == 3:
                    boss_pos["y"] -= 10
                if randin3 == 4:
                    boss_pos["y"] += 10

                if (boss_pos["x"] < -snake_size[0]):
                    boss_pos["x"] = w
                elif (boss_pos["x"] > w):
                    boss_pos["x"] = 0
                elif (odinp_pos["y"] < -snake_size[1]):
                    boss_pos["y"] = h
                elif (odinp_pos["y"] > h):
                    boss_pos["y"] = 0

            if (odinp_pos["x"] < -snake_size[0]):
                odinp_pos["x"] = w
            elif (odinp_pos["x"] > w):
                odinp_pos["x"] = 0
            elif (odinp_pos["y"] < -snake_size[1]):
                odinp_pos["y"] = h
            elif (odinp_pos["y"] > h):
                odinp_pos["y"] = 0
            pygame.draw.circle(display, colors["bhc"], [yad_pos["x"], yad_pos["y"]], 1)
            pygame.draw.circle(display, colors["leh"], [m1_pos["x"], m1_pos["y"]], 10)
            m1_pos = {
                "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
            }
            pygame.draw.circle(display, colors["leh"], [m2_pos["x"], m2_pos["y"]], 10)
            m2_pos = {
                "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
            }
            pygame.draw.circle(display, colors["leh"], [m3_pos["x"], m3_pos["y"]], 10)
            m3_pos = {
                "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
            }
            pygame.draw.circle(display, colors["leh"], [m4_pos["x"], m4_pos["y"]], 10)
            m4_pos = {
                "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
            }
            pygame.draw.circle(display, colors["leh"], [m5_pos["x"], m5_pos["y"]], 10)
            m5_pos = {
                "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
            }
            pygame.draw.rect(display, colors["hel"], [food_pos["x"], food_pos["y"], 10, 10])
            pygame.draw.rect(display, colors["dem"], [sila_pos["x"], sila_pos["y"], 5, 5])

            if snake_pos["x"] == food_pos["x"] and snake_pos["y"] == food_pos["y"]:
                hp += 5
                xp += 10
                print("Your xp - ", xp)
                print("Your hp - ", hp)
                colors["app"] = random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
                food_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
                yad_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10
                }
            if snake_pos["x"] == yad_pos["x"] and snake_pos["y"] == yad_pos["y"]:
                hp -= 100
                print("Green circle")
            if snake_pos["x"] == boss_pos["x"] and snake_pos["y"] == boss_pos["y"]:
                hp -= 70
                print("Your hp - ", hp)
            if snake_pos["x"] == sila_pos["x"] and snake_pos["y"] == sila_pos["y"]:
                sila += 2.5
                xp += 10
                print("Your xp - ", xp)
                print("You are strong!")
                sila_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == h1_pos["x"] and snake_pos["y"] == h1_pos["y"]:
                bhp -= sila
                print("Boss hp - ", bhp)
                h1_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == m1_pos["x"] and snake_pos["y"] == m1_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m1_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == m2_pos["x"] and snake_pos2["y"] == m2_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m2_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == m3_pos["x"] and snake_pos["y"] == m3_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m3_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == m4_pos["x"] and snake_pos["y"] == m4_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m4_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == m5_pos["x"] and snake_pos["y"] == m5_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m5_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos["x"] == odinp_pos["x"] and snake_pos["y"] == odinp_pos["y"]:
                hp -= 20
                print("Your hp - ", hp)

            if snake_pos["x"] == boss_pos["x"] and snake_pos["y"] == boss_pos["y"]:
                hp -= 20
                print("Your hp - ", hp)

            if snake_pos2["x"] == food_pos["x"] and snake_pos2["y"] == food_pos["y"]:
                hp += 5
                xp += 10
                print("Your xp - ", xp)
                print("Your hp - ", hp)
                colors["app"] = random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)
                food_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
                yad_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10
                }
            if snake_pos2["x"] == yad_pos["x"] and snake_pos2["y"] == yad_pos["y"]:
                hp -= 100
                print("Green circle")
            if snake_pos2["x"] == boss_pos["x"] and snake_pos2["y"] == boss_pos["y"]:
                hp -= 70
                print("Your hp - ", hp)
            if snake_pos2["x"] == sila_pos["x"] and snake_pos2["y"] == sila_pos["y"]:
                sila += 2.5
                xp += 10
                print("Your xp - ", xp)
                print("You are strong!")
                sila_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos2["x"] == h1_pos["x"] and snake_pos2["y"] == h1_pos["y"]:
                bhp -= sila
                print("Boss hp - ", bhp)
                h1_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos2["x"] == m1_pos["x"] and snake_pos2["y"] == m1_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m1_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos2["x"] == m2_pos["x"] and snake_pos2["y"] == m2_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m2_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos2["x"] == m3_pos["x"] and snake_pos2["y"] == m3_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m3_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos2["x"] == m4_pos["x"] and snake_pos2["y"] == m4_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m4_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos2["x"] == m5_pos["x"] and snake_pos2["y"] == m5_pos["y"]:
                hp -= 5
                print("Your hp - ", hp)
                m5_pos = {
                    "x": round(random.randrange(0, w - snake_size[0]) / 10) * 10,
                    "y": round(random.randrange(0, h - snake_size[0]) / 10) * 10,
                }
            if snake_pos2["x"] == odinp_pos["x"] and snake_pos2["y"] == odinp_pos["y"]:
                hp -= 20
                print("Your hp - ", hp)

            if snake_pos2["x"] == boss_pos["x"] and snake_pos2["y"] == odinp_pos["y"]:
                hp -= 20
                print("Your hp - ", hp)

            if hp <= 0:
                print("You lose!")
                pygame.quit()
                ex = True
            if bhp <= 0:
                bs = 0
                sleep(1)
                print("You win!")
                pygame.quit()
                ex = True
            if bhp <= 500:
                bs = 25
                FPS = 100
            if bhp <= 250:
                bs = 10
            if bhp <= 70:
                bs = 5
                FPS = 500

            if xp == 100:
                sila += 2
                hp += 50
                xp += 1
                print("New level! (1)")
                print("You are strong!")
                print("Your hp - ", hp)

            if xp == 391:
                xp -= 1

            if xp == 400:
                sila += 3
                hp += 60
                xp += 1
                print("New level! (2)")
                print("You are strong!")
                print("Your hp - ", hp)

            if xp == 791:
                xp -= 1

            if xp == 800:
                sila += 4
                hp += 70
                xp += 1
                print("New level! (3)")
                print("You are strong!")
                print("Your hp - ", hp)

            if xp == 1591:
                xp -= 1

            if xp == 1600:
                sila += 5
                hp += 80
                xp += 1
                print("New level! (4)")
                print("You are strong!")
                print("Your hp - ", hp)

            if xp == 2991:
                xp -= 1

            if xp == 3000:
                sila += 6
                hp += 90
                xp += 1
                print("New level! (5)")
                print("You are strong!")
                print("Your hp - ", hp)

            clock.tick(FPS)
            try:
                pygame.display.update()
            except:
                pass
        pygame.quit()


    root = Tk()
    root.title('MEGA BOSS Launcher')
    root.geometry('200x200')
    root['bg'] = 'white'
    Button(root, text='Start', command=btn, bg='Red', fg='white', font='Arial 25').pack()
    Button(root, text='2-Players', command=btn3, bg='Red', fg='white', font='Arial 25').pack()
    Button(root, text='Exit', command=btn2, bg='Red', fg='white', font='Arial 25').pack()
    root.mainloop()
